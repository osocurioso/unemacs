// unemacs removes all Emacs backup files (*~) in the current
// directory and all its subdirectories.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

var dryrun = flag.Bool("dryrun", false, "only list files that would normally be removed")

func main() {
	flag.Parse()

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	// chan for walkfn to report back all files ending in '~'
	fch := make(chan string)

	// filter out all paths ending in `~`
	walkfn := func(path string, info os.FileInfo, err error) error {
		n := len(path)
		if path[n-1] == '~' {
			fch <- path
		}
		return err
	}
	// traverse subdirectories of `dir` in background goroutine
	go func() {
		err := filepath.Walk(cwd, walkfn)
		if err != nil {
			log.Println(err)
		}
		close(fch)
	}()

	for fn := range fch {
		relfn, err := filepath.Rel(cwd, fn)
		if err != nil {
			log.Println(err)
		}
		if *dryrun {
			fmt.Println(relfn)
		} else {
			os.Remove(relfn)
		}
	}
}
